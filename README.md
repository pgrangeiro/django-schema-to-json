Django schema to json
=====================



Overview
--------------------------------------------------------------------------------------
Create a json file from database schema.
Tested only in MySQL.



About
--------------------------------------------------------------------------------------
It is a Django custom command.
To learn about how add new commands in Django, please see the [Django Documentation][]
[Django Documentation]: https://docs.djangoproject.com/en/dev/howto/custom-management-commands/



Execute
--------------------------------------------------------------------------------------
After the installation:
> python manage.py --app_name=your_app --model_name=your_model --indent=indentation_level

- app_name: It's app which you want export
- model_name: If you want export a especific model you can pass it on this parameter
- indent: The indentation level from json file



Example
--------------------------------------------------------------------------------------
> python manage.py --app_name=your_app --model_name=your_model --indent=indentation_level > test.json
