# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from django.conf import settings
from django.db import connection
from django.core import serializers
from django.contrib.contenttypes.models import ContentType
import json


class Command(BaseCommand):
    # Exporta a estrutura da tabela em formato json

    option_list = BaseCommand.option_list + (
        make_option(
            '--app_name',
            dest='app_name',
            nargs=1,
            help=u'Aplicação a ser exportada'
        ),
        make_option(
            '--model_name',
            dest='model_name',
            nargs=1,
            help=u'Modelo a ser exportado. Deve ser informado em conjunto com --app_name'
        ),
        make_option(
            '--indent',
            dest='indent',
            nargs=1,
            help=u'Nível de indentação do arquivo gerado'
        )
    )

    def handle(self, **options):
        app_name, model_name, indent = None, None, None

        # Preenchendo argumentos
        if options.has_key('app_name'):
            app_name = options['app_name']
        if options.has_key('model_name'):
            model_name = options['model_name']
        if options.has_key('indent') and options['indent']:
            indent = int(options['indent'])

        # Validação de argumentos
        if model_name and not app_name:
            raise CommandError(u'Informe --app_name quando --model_name for informado.')

        cursor = connection.cursor()
        sql = ''
        tables = None
        data, result = {}, {}

        # Pegando tabelas
        if not app_name:
            sql = 'SHOW TABLES'
            cursor.execute(sql)
            tables = cursor.fetchall()
        if app_name and not model_name:
            sql = 'SHOW TABLES WHERE TABLES_IN_%s LIKE "%s_%%%%"' % (settings.DATABASES['default']['NAME'],  app_name)
            cursor.execute(sql)
            tables = cursor.fetchall()
        
        # Pegando dados das tabelas
        if tables:
            for item in tables:
                sql = 'EXPLAIN %s' % (item[0])
                cursor.execute(sql)
                data[item[0]] = cursor.fetchall()
        else:
            sql = 'EXPLAIN %s_%s' % (app_name, model_name)
            cursor.execute(sql)
            data['%s_%s' % (app_name, model_name)] = cursor.fetchall()
        
        # Preparando dict
        if data:
            for key in data.keys():
                fields = []

                # Verificando se existe informação do ContentType sobre a tabela
                temp_model = [item for item in ContentType.objects.all() if str(key)=='%s_%s' % (item.app_label, item.model)]
                if temp_model:
                    temp_model = temp_model[0].model_class()

                for item in data[key]:
                    field = {}
                    field['name'] = item[0]
                    field['label'] = ''
                    if temp_model:
                        label = [f.name for f in temp_model._meta.fields if f.get_attname()==item[0]] 
                        if label:
                            field['label'] = label[0]                    
                    field['type'] = item[1]
                    field['nullable'] = item[2]
                    field['key'] = item[3]
                    field['default'] = item[4]
                    field['info'] = item[5]
                    fields.append(field)    

                if not isinstance(temp_model,list):
                    label = temp_model._meta.verbose_name
                    if not isinstance(label, str) and not isinstance(label, unicode):
                        label = unicode(temp_model._meta.verbose_name)
                else:
                    label = ''
                result[key] = {
                    'label': label,
                    'fields': fields
                }
            print json.dumps(result, indent=indent)

